% THIS SCRIPT IS USED TO PERFORM A BATCH RUN OF AN ENERGY HUB MODEL IMPLEMENTED IN AIMMS.
% THE IMPLEMENTATION HERE IS FOR RUNNING AN ENERGY HUB MODEL WITH VARYING NUMBERS OF BUILDINGS INCLUDED IN THE ENERGY HUB.
% OUTPUT IN THIS CASE IS THE TOTAL COST, STORAGE SIZE AND NUMBER OF INSTALLED SOLAR PANELS OF AN ENERGY HUB WITH WITH A VARYING NUMBER OF BUILDINGS.

%% SET THE NECESSARY PARAMETERS
clear;
clc;
close all;

number_of_buildings = 50; %how many buildings should be included in the energy hub
number_of_timesteps_in_BEM_results = 525600; %number of timesteps in the results of the building energy model
number_of_combinations = 1; %the number of building combinations to try

project_path = 'C:\Users\boa\Documents\Testing\EnergyHub'; %the path to the AIMMS project
model_name = 'EnergyHub'; %the name of the AIMMS project
aimms_input_path = 'C:\Users\boa\Documents\Testing\EnergyHub\'; %input path of the relevant AIMMS files
results_path = 'C:\Users\boa\Documents\Testing\Results\'; %where should the results be saved

%% RUN THE ENERGY HUB MODEL

%load the BEM output data
load([results_path 'BEM_output_data_50Buildings.mat']);

%test varying numbers of buildings
storage_capacity_per_building_combined = zeros(number_of_combinations,number_of_buildings);
electricity_cost_combined = zeros(number_of_combinations,number_of_buildings);
total_cost_combined = zeros(number_of_combinations,number_of_buildings);
total_carbon_combined = zeros(number_of_combinations,number_of_buildings);
electric_heaters_installed_combined = zeros(number_of_combinations,number_of_buildings);
solar_thermal_installed_combined = zeros(number_of_combinations,number_of_buildings);
heat_demand_combined = [];
electricity_demand_combined = [];
for buildings_in_energy_hub=1:number_of_buildings
    
    disp(['CALCULATING: ' num2str(buildings_in_energy_hub) ' buildings in energy hub']);
    
    building_combinations = [];
    for i=1:number_of_combinations
        building_combinations = vertcat(building_combinations, randperm(50,buildings_in_energy_hub));
    end
    
    storage_capacity_per_building = zeros(length(building_combinations(:,1)),1);
    electricity_cost = zeros(length(building_combinations(:,1)),1);
    total_cost = zeros(length(building_combinations(:,1)),1);
    total_carbon = zeros(length(building_combinations(:,1)),1);
    electric_heaters_installed = zeros(length(building_combinations(:,1)),1);
    solar_thermal_installed = zeros(length(building_combinations(:,1)),1);
    heat_demand_this_combination = [];
    electricity_demand_this_combination = [];
    for j=1:length(building_combinations(:,1))
        
        %load the data from the building energy models - the faster way
        heat_demand = zeros(number_of_timesteps_in_BEM_results,1);
        electricity_demand = zeros(number_of_timesteps_in_BEM_results,1);
        disp(['Evaluating building combination ' num2str(building_combinations(j,:))]);
        for i=1:length(building_combinations(1,:))
            heat_demand = heat_demand + heat_demand_matrix(:,(building_combinations(j,i)));
            electricity_demand = electricity_demand + electricity_demand_matrix(:,(building_combinations(j,i)));
        end

        %convert from joules to kWh
        heat_demand = heat_demand * 2.77777778e-07;
        electricity_demand = electricity_demand * 2.77777778e-07;
        
        disp(['Number of buildings in energy hub = ' num2str(buildings_in_energy_hub)]);
        disp(['Max heat demand = ' num2str(max(heat_demand))]);
        disp(['Max heat demand per building = ' num2str(max(heat_demand) / buildings_in_energy_hub)]);
        disp(['Max electricity demand = ' num2str(max(electricity_demand))]);
        disp(['Max electricity demand per building = ' num2str(max(electricity_demand) / buildings_in_energy_hub)]);
        
        %csvwrite([aimms_input_path, 'HeatingDemandData_' num2str(i) 'Buildings.csv'],[heat_demand electricity_demand]);
        xlswrite([aimms_input_path, 'HeatingDemandData.xlsx'],[heat_demand electricity_demand]);
        xlswrite([aimms_input_path, 'NumberOfBuildings.xlsx'],buildings_in_energy_hub);

        % RUN THE MODEL
        % the project must contain the commands to load the data CaseFileLoad(somefile.data) and not to save (in MainTermination change return DataManagementExit(); to return 1;)
        % the options file in the MainProject/Settings folder should contain the text "solution_listing 3" so that the results are written to the lis file.
        fprintf('Running AIMMS \n');
        tic

        [status.run,out] = system(['"C:\Program Files\AIMMS\AIMMS 4\Bin\aimms.exe" -m --run-only MainExecution "' project_path '\' model_name '.aimms"']);

        fprintf(' Done (%g seconds).\n',toc); % report

        % LOAD IN THE RELEVANT RESULTS DATA
        filename = [project_path '\log\' model_name '.lis'];
        fileID = fopen(filename);
        s1 = textscan(fileID, '%s', 'delimiter', '\n');
        %s2 = s1{1,1}(find(strncmp(s1{1,1},'StoreCap',7)));
        storecap1 = s1{1,1}(8,:);
        storecap2 = textscan(char(storecap1), '%s');
        storecap3 = str2num(storecap2{1}{3});
        eleccost1 = s1{1,1}(9,:);
        eleccost2 = textscan(char(eleccost1), '%s');
        eleccost3 = str2num(eleccost2{1}{3});
        totalcost1 = s1{1,1}(14,:);
        totalcost2 = textscan(char(totalcost1), '%s');
        totalcost3 = str2num(totalcost2{1}{3});
        totalcarbon1 = s1{1,1}(15,:);
        totalcarbon2 = textscan(char(totalcarbon1), '%s');
        totalcarbon3 = str2num(totalcarbon2{1}{3});
        eh1 = s1{1,1}(20,:);
        st1 = s1{1,1}(21,:);
        eh2 = textscan(char(eh1), '%s');
        eh3 = str2num(eh2{1}{3});
        st2 = textscan(char(st1), '%s');
        st3 = str2num(st2{1}{3});
        fclose(fileID);
        
        disp(buildings_in_energy_hub);
        storage_capacity_per_building(j,1) = storecap3 / buildings_in_energy_hub;
        electricity_cost(j,1) = eleccost3 / buildings_in_energy_hub;
        total_cost(j,1) = totalcost3 / buildings_in_energy_hub;
        total_carbon(j,1) = totalcarbon3;
        electric_heaters_installed(j,1) = eh3;
        solar_thermal_installed(j,1) = st3;
       
        heat_demand_this_combination = horzcat(heat_demand_this_combination, heat_demand);
        electricity_demand_this_combination = horzcat(electricity_demand_this_combination, electricity_demand);
        
        disp(['Storage capacity (total) = ' num2str(storage_capacity_per_building(j,1) * buildings_in_energy_hub)]);
        disp(['Storage capacity (per building) = ' num2str(storage_capacity_per_building(j,1))]);
        disp(['Solar thermal installed (total) = ' num2str(solar_thermal_installed(j,1))]);
        disp(['Solar thermal installed (per building) = ' num2str(solar_thermal_installed(j,1) / buildings_in_energy_hub)]);
    end
    
    heat_demand_combined = horzcat(heat_demand_combined, mean(heat_demand_this_combination,2));
    electricity_demand_combined = horzcat(electricity_demand_combined, mean(electricity_demand_this_combination,2));
    
    storage_capacity_per_building_combined(:,buildings_in_energy_hub) = storage_capacity_per_building;
    electricity_cost_combined(:,buildings_in_energy_hub) = electricity_cost;
    total_cost_combined(:,buildings_in_energy_hub) = total_cost;
    total_carbon_combined(:,buildings_in_energy_hub) = total_carbon;
    electric_heaters_installed_combined(:,buildings_in_energy_hub) = electric_heaters_installed;
    solar_thermal_installed_combined(:,buildings_in_energy_hub) = solar_thermal_installed;
end

%save the results data
save([results_path, 'aggregate_demand_data.mat'],'heat_demand_combined','electricity_demand_combined');
save([results_path, 'results_data.mat'],'storage_capacity_per_building_combined','electricity_cost_combined','total_cost_combined','total_carbon_combined','electric_heaters_installed_combined','solar_thermal_installed_combined');
csvwrite([results_path, 'Results_StorageCapacityPerBuilding.csv'],storage_capacity_per_building_combined);
csvwrite([results_path, 'Results_TotalSolarThermalInstalled.csv'],solar_thermal_installed_combined);

disp('FINISHED!');


