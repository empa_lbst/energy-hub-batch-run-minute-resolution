# Description #
This model is a modified version of the energy hub model developed by Akomeno (2015), modified to operate at one minute resolution and to enable testing of energy hubs with varying numbers of buildings. The model takes the results of a building energy model as input and calculates an optimal operation schedule for energy hubs of different sizes. It is assumed that each building can be outfitted with solar thermal panels, which (via a district heating network) connect to a centralized heat storage. The aim is to identify a system configuration that minimizes total costs (investment + maintenance + energy) given the goal of cutting carbon emissions to 25% of their baseline level. 

The energy hub model is implemented in the optimization package Aimms as a mixed integer linear program.  The outputs of the model include the operation schedule of the energy hub and optimal sizing of the heat storage and solar thermal installations. The calculations account for the investment, maintenance and operational costs of the solar thermal and storage installations, as well as of the grid-connected electric heaters that are assumed to supplement these.  A single run of the model represents a timeframe of one week in April at one minute time resolution.

# How can I use this module? #
This module was developed for the purpose of testing the effects of varying numbers of buildings on the optimal technical configuration of an energy hub. The module features dependencies which are currently not included with the module, namely the results of a series of building energy simulations. For this reason, it is not possible to run the module "out of the box".  However, the implemented code may be useful for researchers seeking to carry out batch runs of energy hub models, specifically the content of Run_AIMMS.m.  Additionally, the EnergyHub.ams file may be used as a template for the development of other energy hub models.

# Necessary software #
The module requires Matlab and Aimms.

# How does it work? #
Run_AIMMS.m sets the necessary parameters, calls different formulations of the the energy hub model (EnergyHub.ams) in sequence, and processes the results by parsing the contents of the generated LIS file.